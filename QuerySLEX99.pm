package QuerySLEX99;

use WWW::Mechanize;
#use HTML::Parser;
use strict;
use Encode qw/from_to/;

sub new {
    my $class = shift;

    my $a = WWW::Mechanize->new(onwarn => undef, onerror => undef);

    my $self = bless {
	_server => 'http://www.forma.sk/onlines/slex/index.asp',
	_agent  => $a,
    }, $class;

    return $self;
}

my $re_adjdeverb1=qr/an[�y]/;
my $term_t1="a�";
my $re_adjdeverb2=qr/en[�y]/;
my $term_t2="i�";
my $re_adjdeverb3=qr/nut[�y]/;
my $term_t3="nu�";
my $re_t=qr/�/;

sub search {
    my ($self, $query) = @_;

    from_to($query,"utf-8","windows-1250");

    warn "No query given, aborting" and return unless $query;

    $self->{_agent}->get($self->{_server});

    $self->{_agent}->submit_form(
	 form_number => 1,
	 fields      => {
	     slovo => $query
	 }
    );

    my $ok=0;
    my @answers=();
    my $check_and_print = sub {
	my ($f,$t,$c) = @_;
	if ($f eq $query || $f=~/^$query s[ai]$/) {
	    from_to($f,"windows-1250","utf-8");
	    from_to($c,"windows-1250","utf-8");
	    from_to($t,"windows-1250","utf-8");
	    if ($c=~/^.*dok\..*$/) {
		$c="v-";
	    } elsif ($c=~/^ž\.$/) {
		$c="nc-f";
		if ($t=~/-y/) {$c.="dv"}
		elsif ($t=~/-ie/ && $f=~/ia$/) {$c.="iv"}
		elsif ($t=~/-e/ && $f=~/[aeiouyáéíóúý]$/) {$c.="mv"}
		elsif ($t=~/-e/) {$c.="m1"}
		elsif ($t=~/-i/) {$c.="m2"}
		$t="OK";
	    } elsif ($c=~/^m\.$/) {
		$c="nc-m";
		if ($t=~/-ovia/) {$c.="av";$t="OK";}
		elsif ($t=~/-u/) {$c.="idu";$t="OK";}
		elsif ($t=~/-a/) {$c.="";$t="OK";}
	    } elsif ($c=~/^s\.$/) {
		$c="nc-n";
		if ($t=~/-a/ && $f=~/o$/) {$c.="o"}
		elsif ($t=~/-a/ && $f=~/e$/) {$c.="e"}
		elsif ($t=~/-ia/) {$c.="ie"}
		elsif ($t=~/-aťa/) {$c.="a"}
		$t="OK";
	    } elsif ($c=~/^príd\.$/) {
		$c="adj";
	    }
	    if ($c ne "čast." && $c !~/čisl/ && $c ne "prísl.") {
		my $line="$f\t$t\t$c";
		push (@answers,$line);
	    }
	}
    };
    my $reponse=$self->{_agent}->content();
    my $temp="";
    $reponse=~s/\r//g;
    for (split(/(\n|<\/[Pp]>)/,$reponse)) {
	if (/^ *<div id=\"slovo\"> *$/) {
	    $ok=1;
	} elsif ($ok==1) {
#	    print STDERR "$_\n";
	    if (/<\/div> *$/) {
		last;
	    } elsif (/ nie je v slov/) {
		$ok=-1;
	    } elsif (/<FONT[^<]*<[bB]> *([^<]+)<.*<\/FONT> *([^<]*) *\<SMALL\> *([^<]*) *$/){
		$check_and_print->($1,$2,$3);
	    } elsif (/^<P><B> *([^<]*) *<\/B> *([^<]*[^ <])? *\<SMALL\> *([^<0-9]*)[ <]/){
		$check_and_print->($1,$2,$3);
	    } elsif (/<FONT[^<]*<[bB]> *([^<]+)($|<)/){
		$temp=$1;
	    } elsif ($temp!~"" && /^<\/[bB]><\/FONT> *(?:<P>)? *(.*[^ <])? *\<SMALL\> *([^<0-9]*)($|[ <])/) {
		$check_and_print->($temp,$1,$2);
		$temp="";
	    } elsif (/<SMALL><P>([^<]+)<\/SMALL> *<[bB]>([^<]+).* *([^<>]*) *$/){
		$check_and_print->($2,$3,$1);
	    }
	} elsif ($ok==-1 && $query!~/ /) {
	    if ($query=~/$re_adjdeverb1$/) {
		my $newquery=$query;
		$newquery=~s/$re_adjdeverb1$/$term_t1/;
		from_to($newquery,"windows-1250","utf-8");
		@answers=$self->search($newquery);
		from_to($query,"windows-1250","utf-8");
		if ($#answers>-1) {push (@answers,$query."\tadj-l\tadj");}
	    } elsif ($query=~/$re_adjdeverb2$/) {
		my $newquery=$query;
		$newquery=~s/$re_adjdeverb2$/$term_t2/;
		from_to($newquery,"windows-1250","utf-8");
		@answers=$self->search($newquery);
		from_to($query,"windows-1250","utf-8");
		if ($#answers>-1) {push (@answers,$query."\tadj-l\tadj");}
	    } elsif ($query=~/$re_adjdeverb3$/) {
		my $newquery=$query;
		$newquery=~s/$re_adjdeverb3$/$term_t3/;
		from_to($newquery,"windows-1250","utf-8");
		@answers=$self->search($newquery);
		from_to($query,"windows-1250","utf-8");
		if ($#answers>-1) {push (@answers,$query."\tadj-l\tadj");}
	    } elsif ($query=~/$re_t$/) {
		from_to($query,"windows-1250","utf-8");
		@answers=$self->search($query." sa");
		push(@answers,$self->search("$query si"));
	    }
	    last;
	}
    }
#    for (0..$#answers) {
#	print STDERR $answers[$_]."\n";
#    }
    from_to($query,"windows-1250","utf-8");
    if ($ok==1 && $#answers==-1) {push(@answers,"$query\t???\t???")}
    return @answers;
}
1;
