#!/usr/bin/env perl

use strict;
use QueryGoogleSK;

my $answer=();
my %pastanswer=();

$|=1;

open(GOOGLELOG,"< googlelog") || die("Impossible d'ouvrir googlelog en lecture\n");
while(<GOOGLELOG>) {
    chomp;
    if (/^([^\t]*)\t(.*)/) {
	$pastanswer{$1}=$2;
    }
}
close(GOOGLELOG);

open(GOOGLELOG,">> googlelog") || die("Impossible d'ouvrir slexlog en ecriture\n");
while(<>) {
    chomp;
    if (defined($pastanswer{$_})) {
	$answer=$pastanswer{$_};
    } else {
	$answer=QueryGoogleSK->googly($_);
    }
    print GOOGLELOG "$_\t$answer\n";
    print "$answer\n";
}
close(GOOGLELOG);
