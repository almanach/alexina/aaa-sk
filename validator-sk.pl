#!/usr/bin/env perl

use strict;
use QuerySLEX99;

my $slex=QuerySLEX99->new();
my @answers=();
my %pastanswers=();
my $lemme;
my $classe;
my $proba;
my $occ;

$|=1;

open(SLEXLOG,"< slexlog") || die("Impossible d'ouvrir slexlog en lecture\n");
while(<SLEXLOG>) {
    chomp;
    /^([^\t]*)\t(.*)/;
    if ($2 ne "NONEXISTENT") {
	push(@{$pastanswers{$1}},$_);
    } else {
	@{$pastanswers{$1}}=();
    }
}
close(SLEXLOG);

open(SLEXLOG,">> slexlog") || die("Impossible d'ouvrir slexlog en ecriture\n");
my $n=0;
while(<>) {
    if (/^([^\t0-9\*\+\-][^\t0-9]*)\t([^\t]*)\t-\t([^\t]*)\t([^\t]*)\t(.*)$/ && $n<50) {
	$lemme=$1;
	$classe=$2;
	$proba=$3;
	$occ=$4;
	if ($proba>0.9 && $occ>0) {
	    if (defined($pastanswers{$1})) {
		@answers=@{$pastanswers{$1}};
	    } elsif (defined($pastanswers{$1." sa"})) {
		@answers=@{$pastanswers{$1." sa"}};
	    } else {
#		print STDERR ".";
		@answers=$slex->search($lemme);
		$n++;
		if ($#answers==-1) {
		    print SLEXLOG $lemme."\tNONEXISTENT\n";
		    print STDERR $lemme."\tNONEXISTENT\n";
		} else {
		    for (0..$#answers) {
			print SLEXLOG $answers[$_]."\n";
			print STDERR $answers[$_]."\n";
		    }
		}
	    }
	    if ($#answers==-1) {
		print "*";
	    } else {
		for (0..$#answers) {
		    print STDERR "< $answers[$_]\n";
		    $answers[$_]=~/^(.*)\t(.*)\t(.*)$/;
		    my $answer_l=$1; my $answer_f=$2; my $answer_c=$3;
		    if ($answer_l eq $lemme) {
			if ($answer_c eq $classe) {print "+";}
			elsif ($answer_c eq "adj" && $classe=~/^adj/) {print "+";}
			elsif ($answer_f eq "OK" || ($answer_c=~/^(nc-|v-)/ && $classe!~/^$answer_c/)) {print "*";}
		    }
		}
	    }
	}
    }
    print "$_";
}
close(SLEXLOG);

